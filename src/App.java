public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");
        // Khởi tạo object (đối tượng) kiểu class string
        System.out.println("CHAO MUNG DEN VOI DEVCAMP 120");
        // In (println) đối tượng này ra Terminal
        String appName = "Quyet tam thanh lap trinh vien!";
        System.out.println(appName);
        System.out.println("lowercase: " + appName.toLowerCase());
        System.out.println("UPPERCASE: " + appName.toUpperCase());
    }
}
